import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class StringsReverser {

    public static void main(String[] args) {
        String inputFileName = "";
        String outputFileName = "";
        if(args.length < 3) {
            try {
                inputFileName = args[0];
                outputFileName = args[1];
            } catch(ArrayIndexOutOfBoundsException e) {
                System.err.println("Wrong number of parameters");
                System.exit(0);
            }
            try {
                File inputFile = new File(inputFileName);
                File outputFile = new File(outputFileName);

                InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(inputFile), StandardCharsets.UTF_8);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

                String line;
                char character;

                while((line = bufferedReader.readLine()) != null) {
                    String outputString = "";
                    for(int i = 0; i < line.length(); i++) {
                        character = line.charAt(i);
                        outputString = character + outputString;
                    }
                    bufferedWriter.write(outputString);
                    bufferedWriter.newLine();
                }
                bufferedReader.close();
                bufferedWriter.close();
                inputStreamReader.close();
                outputStreamWriter.close();

            } catch(FileNotFoundException e) {
                System.err.println("Wrong inputFile name");
                e.printStackTrace();
                System.exit(0);
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
        else {
            System.err.println("Too many parameters");
        }

    }
}
